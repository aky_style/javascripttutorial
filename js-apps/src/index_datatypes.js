//variables
let firstName = "Akshay"
let lastName = "Goel"

console.log("My Name - " + firstName + " " + lastName);

//es 6 template literal
console.log(`My Name - ${firstName} ${lastName}`);

let isWorking = true;
console.log(`Are you working - ${isWorking}`);

let age;
console.log(`My Age - ${age}`);

let ageStatus = age * 2;
console.log(`Age Status - ${ageStatus}`)

let res = ageStatus / 0;
console.log(`NaN / 0 =  ${res}`);
//avg :Infinity div/ 0
let avg = 100 / 0;
console.log(`Avg ${avg}`);

const x = 100
console.log(x)
//This will give an error 
//x = 90
