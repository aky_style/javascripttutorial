//return functions

function counter(){
    function increment(){        return `Increment Function`;    }
    return increment();
}

function counter1(){
    return function(){        return `Increment Function`;    }
}

console.log(counter());
console.log(counter1()());

function a(){
    return function(name){
        return function(bday){
            return function(age){
                return `${name} ${bday} ${age}`;
            }
        }
    }
}
console.log(a() (`Akshay`)(`10-05-1990`) (`29`));

//var b = name=>bday=>age=>`${name} ${bday} ${age}`;
//console.log(b() (`Akshay`)(`10-05-1990`) (`29`));

var b =()=> name=>bday=>age=>`${name} ${bday} ${age}`;
const res=b()(`Akshay`)(`10-05-1990`) (`29`);
console.log(res);
