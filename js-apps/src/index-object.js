function Employee(){
    return 10;
}

console.log(Employee());
console.log(window.Employee());

function Employee1(){
    this.id = 1;
    this.name = `Akshay`;
    this.salary = `$100000`;
}
var emp1 = new Employee1();
console.log(emp1.id);

function Employee2(id, name, salary){
    this.id = id;
    this.name = name;
    this.salary = salary;
}
var emp2 = new Employee2(1,`Akshay`, `$10000000`)
console.log(emp2.id);
emp2.id = 2
console.log(emp2.id);


//ES6 class
class Employee3{
    constructor(id, name, salary){
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    calc(){
        return this.id;
    }
}

var emp4 = new Employee3(1,`Akshay`, `$10000000`)
var emp5 = new Employee3(2,`Akshay`, `$10000000`)
console.log(emp4.calc());
console.log(emp5.calc());

//literal pattern
var emp6 =  {
    id: 2,
    name: `Akshay`,
    salary: `$99999999999999`
};
console.log(`${emp6.id} ${emp6.name} ${emp6.salary}`);

