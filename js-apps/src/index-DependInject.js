class Product{
    constructor(id = 1, name = 'Default' ){
        this.id = id;
        this.name = name;
    }
}

class Order{
    constructor(product = new Product()){
        this.product = product;
    }
}

var p = new Product();
var o = new Order(p);       //Dependency Injection
console.log(o.product.id, o.product.name);