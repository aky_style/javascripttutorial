//function Literals


//function declaration
function sayGreet() {
    console.log("Hey There!!!");
}

//function invocation
const gr = sayGreet;

gr();

//anonymous functions
var hello = function(){
    console.log(`Hello1`);
};

hello();

//Arrow function
hello = () => {
    console.log(`Hello2`);
};

hello();

//Shorter form of the fucntion
hello = (name, age=28) => console.log(`Name : ${name} and age : ${age}`);
hello(`Akshay`,23);

//Short form of the return 
hello = (name, age=28) => `Name : ${name} and age : ${age}`;
console.log(hello(`Akshay`));