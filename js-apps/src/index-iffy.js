//Immediately Invoked function Expressions

(function() {
    console.log(`Akshay`)
}());


//iife with arg
(function(app) {
    console.log(`${app} is there`)
} (`Apple`));

//iife with arg and return
const a = (function(app) {
    return `${app} is there`;
} (`Apple`));
console.log(a);

//arrow version
const b = (app => {
    return `${app} is there`;
}) ('Akshay');
console.log(b);
