//function as parameter

function sayGreet(name= `Akshay`, greeter){
    console.log(greeter(name));
}

function foo(name){
    return `Hi ${name}`;
}

const hi = foo;

//sayGreet(`Akshay`,hi);


var hello = (name, fun2) => fun2(name);
var r = (name) => {
    console.log(name);
}

hello(`Akshay`,r);



/* Trainer's code

//function as parameter

/* function sayGreet(name = 'default', greeter) {
    console.log(greeter(name));
}
const hai = function (name) {
    //console.log(`Hai ${name}`);
    return `Hai ${name}`;
};
sayGreet('subramanian', hai); 

// function sayGreet(greeter) {
//     // greeter('subramanian');
//     console.log(greeter(name));
// }
// sayGreet(function (name) {
//     //console.log(`Hai ${name}`);
//     return `Hai ${name}`;
// });

const sayGreet = () => console.log(greeter(name));
sayGreet(name => `Hai ${name}`);

*/