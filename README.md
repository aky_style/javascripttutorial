# JavaScriptTutorial

__Javascript engines__

1.	Chrome	-	v8 engine




__Index__

1.	JS Fundamentals
2.	JS Functions
3.	JS obj
4.	JS 
5.	JS DOM


__ES5 & ES6__

1.	Language Fundamentals
	variables, literals, loops, control structures, operators
2.	Literals-
	a.	number - 64 bit aka double
	b.	string - "",''
	c.	boolean - true | false
	d.	undefined
	e.	NaN
	f.	Infinity
	g.	function
	h.	null
	i.	object


__Design pattern__

Namespace
AMD - async module design
CommonJS

ES6 module design pattern


__Notes:__
1. WebPack is a bundler/loader for JS
2. JS stores object as a dictionary

**Intialization flow**
1. Global Object is hosted on browser (hosting env)
   1. Window


__Inbuilt Host Objects__

1.	Browser Host:
	1.	POJO:
		1.	Wrapper Objets = NUmber, String, Boolean, Object
		2.	Date, Math, JSON, Regex...........
	2. DOM/ BOM:
      	1.  window, history, location, frame...
      	2.  document,EVery html element
1.  Node Host:
    1.  process
    2.  POJO:
		1.	Wrapper Objets = NUmber, String, Boolean, Object
		2.	Date, Math, JSON, Regex...........

__How to Create Object__
1. Using fucntions - constructor pattern
2. Using ES6 class - constructor pattern
3. Using {} - literal pattern

properties are declared using "this" keyword

__function invocation pattern:__
1.	object invocation -> stack frame
2.	Constructor invocation -> 

__Object Property Manipulation__
1.	Hardcoded
2.	Constructors Parameters

__Types of Object__
1.	Domain Object
	Object created by new operator - Active heap
2.	function object
	Every Function is object, instance of "Function"

__Inheritance:__
	OBjective: Code Reuse

__How to link 2 objects__
Child.prototype = Object.create(Parent.prototype);
